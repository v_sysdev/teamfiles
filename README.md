# Systems Development - Green Team
Working files will be worked on from google drive. Any backups and finished versions of the files will be versionned and stored on the GitHub repo in the 'res' folder.
All templates, meeting agendas and notes, and reports can be accessed from here.

## Meeting Agendas/Notes
All meeting word documents will be linked here.

1. [Initial Meeting](https://docs.google.com/document/d/1YftXb_R4hjw7c0SjjdDTV4unvvWfEDlulj39-UMDnxE/edit?usp=sharing), Jan 24, 2020
2. [Client Search Follow-Up Meeting](https://docs.google.com/document/d/1S55eF6tvIf-VpQbW920L4R6p1tqHiogS-aFhVACLAKY/edit?usp=sharing), Jan 28, 2020
3. [Client Update, Discuss Potential Clients](https://docs.google.com/document/d/1HLljVhl1lf8JKaRJAsbRH7C09SP1Nf_BPaz5vEIJVsA/edit?usp=sharing), Jan 31, 2020
4. [First Client Meeting Summary](https://docs.google.com/document/d/1LSkPRZteTNX33dWX8QVS-XkE-zVmtRXnPNGdVQE-Jwc/edit?usp=sharing), Feb 4, 2020

## Files
Files to be shared and worked on as a group throughout the project.

* [Project plan - 2020-01-31.mpp](https://drive.google.com/open?id=1urjf8DOsxLCV9lgybYgzLgLyIQ5q3slM)
   * Time estimates in the plan are extremely rough and no resources have been assigned to tasks.
* [Project plan - Task Descriptions Document](https://docs.google.com/document/d/1O3CSe-8mpXy4JbQT-8EgclI9LaL6j14J1jH89OjGNak/edit?usp=sharing)
   * This file is currently a WIP, with only the 1st deliverable being completely described.

## Reports
All written reports will be linked here for every deliverable.

|No.|Deliverable Name|Due Date|Lead|Link
|---|----------------|--------|----|----|
|1  |Project Plan|Feb 5, 2020|Tristan|[Link](https://docs.google.com/document/d/1ogJE0sHZpOelf_Ret5zgoCxpRW_VES6G-PVkjSTrA0Y/edit?usp=sharing)
|2  |Client and business domain summaries, questionnaire|Feb 12, 2020|Haymond|[Link](https://docs.google.com/document/d/1AmWQ-jKaMZc8hWUDbRlRskhmSBHBG9vEblMLTHM_qds/edit?usp=sharing)
|3  |Use cases and UML Diagrams|Feb 25, 2020|Xiang|[Link](https://docs.google.com/document/d/1URtJRdiSpSGxfxZHFrP4LnFoT0GQMwN_XVXvs45tjOU/edit?usp=sharing)
|4  |User stories|Mar 25, 2020|James|[Link]()
|5  |Prototype UI and client comments|Apr 7, 2020|Synthia|[Link]()
|6  |Database design|Apr 21, 2020|Haymond|[Link]()
|7  |Implementation and client comments|May 8, 2020|Tristan|[Link]()

## Templates
All templates used for meetings/reports will be here.

1. [Report/Cover Page Template](https://docs.google.com/document/d/1OwhacH2FHw6TUT56dmdQ8gZMzO00ScpSv9P0zFT2nHw/edit?usp=sharing)
2. [Meeting/Agenda Template](https://docs.google.com/document/d/1Jr9nIwXakM2yRtO8-iuZ0q2T5qW9Tq6WGon1_e6_lBw/edit?usp=sharing)
